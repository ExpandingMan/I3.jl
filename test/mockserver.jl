using I3

#ENV["JULIA_DEBUG"] = "I3"


function mockworkspaces()
    [I3.Workspace(0, 0, "mock_workspace_0", false, false, I3.Rectangle(0, 0, 2560, 1440), "DP-1", false),
     I3.Workspace(1, 1, "mock_workspace_1", true, true, I3.Rectangle(0, 0, 2560, 1440), "DP-1", false)
    ]
end
function mockoutputs()
    [I3.Output("DP-1", true, true, I3.Rectangle(0, 0, 2560, 1440), "mock_workspace_0"),
     I3.Output("DP-2", false, false, I3.Rectangle(0, 0, 2560, 1440), nothing)
    ]
end

mockhandler() = I3.MockHandler(mockworkspaces(), mockoutputs(), nothing)

mockserve(sockname="testsock", h=mockhandler()) = I3.serve(h, sockname)
