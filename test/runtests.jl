using I3
using Test, JSON3

include("mockserver.jl")

const TEST_SOCKET = joinpath(dirname(pathof(I3)),"..","test","testsock")


@testset "I3.jl" begin
    @testset "ipc_noserver" begin
        @test I3.pack(0) == vcat(I3.MAGIC_BYTES, reinterpret(UInt8, Int32[0, 0]))
        @test I3.pack(1, "message") == vcat(I3.MAGIC_BYTES, reinterpret(UInt8, Int32[7, 1]), codeunits("message"))
        str = JSON3.write([Dict("a"=>1)])
        @test I3.pack(2, str) == vcat(I3.MAGIC_BYTES, reinterpret(UInt8, Int32[ncodeunits(str), 2]), codeunits(str))

        io = IOBuffer()
        I3.send(io, 0)
        @test take!(io) == vcat(I3.MAGIC_BYTES, reinterpret(UInt8, Int32[0, 0]))
        I3.send(io, 1, "message")
        @test take!(io) == vcat(I3.MAGIC_BYTES, reinterpret(UInt8, Int32[7, 1]), codeunits("message"))

        io = IOBuffer()
        write(io, I3.MAGIC_BYTES, Int32[0, 0]); seekstart(io)
        @test I3.receivebytes(io) == (0, UInt8[])
        io = IOBuffer()
        write(io, I3.MAGIC_BYTES, Int32[4, 1], "test"); seekstart(io)
        @test I3.receivebytes(io) == (1, b"test")
        io = IOBuffer()
        str = JSON3.write([Dict("a"=>1)])
        write(io, I3.MAGIC_BYTES, Int32[ncodeunits(str), 2], str); seekstart(io)
        @test I3.receive(io) == (2, JSON3.read(str))
    end

    @info("starting mock server; will run for remainder of tests")
    @async mockserve(TEST_SOCKET)
    sleep(0.1)  # this is horrible, but shouldn't be needed and is here just in case
    cnxn = I3.Connection(TEST_SOCKET)

    @testset "ipc" begin
        @test isopen(cnxn)

        @test I3.sendreceive(cnxn, 0, "focus up") == JSON3.read("[{\"success\": true}]")
        @test I3.sendreceive(cnxn, 0, "smart_borders on") == JSON3.read("[{\"success\": true}]")
    end

    @testset "commands" begin
        @test isnothing(I3.command(cnxn, "focus up"))

        @test occursin("class=", I3.focus_string("up", class="firefox"))

        @test isnothing(I3.focus(cnxn))

        @test occursin("--no-startup-id", I3.exec_string("ls", no_startup_id=true))
        @test isnothing(I3.exec(cnxn, `ls`))

        @test I3.split_string("vertical") == "split vertical"
    end

    @testset "workspace" begin
        ws = I3.workspaces(cnxn)
        @test ws[1] == I3.Workspace(0, 0, "mock_workspace_0", false, false, I3.Rectangle(0, 0, 2560, 1440), "DP-1", false)
        @test ws[2] == I3.Workspace(1, 1, "mock_workspace_1", true, true, I3.Rectangle(0, 0, 2560, 1440), "DP-1", false)
    end

    @testset "output" begin
        os = I3.outputs(cnxn)
        @test os[1] == I3.Output("DP-1", true, true, I3.Rectangle(0, 0, 2560, 1440), "mock_workspace_0")
        @test os[2] == I3.Output("DP-2", false, false, I3.Rectangle(0, 0, 2560, 1440), nothing)
    end

    if @isdefined(cnxn)
        @info("closing mock server connection")
        close(cnxn)
    end
end

issocket(TEST_SOCKET) && rm(TEST_SOCKET)
