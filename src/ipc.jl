"""
    socketpath()

Get the i3 session socket path by calling `i3 --get-socketpath`.
"""
socketpath() = readchomp(`i3 --get-socketpath`)

"""
    Connection

UNIX socket connection to a running i3 session.  `Connection()` will open a connection to the default i3 session conneciton
obtained with the command `i3 --get-socketpath`.  In most use cases this is likely to be the only valid connection.
"""
struct Connection
    sock::Base.PipeEndpoint
end
Connection(path::AbstractString) = Connection(connect(path))
Connection() = Connection(socketpath())

Base.close(cnxn::Connection) = close(cnxn.sock)
Base.isopen(cnxn::Connection) = isopen(cnxn.sock)

"""
    pack

Prepare a message to be sent to the i3 session via IPC.
"""
pack(mtype::Integer, x::AbstractVector{UInt8}) = vcat(MAGIC_BYTES, reinterpret(UInt8, [Int32(length(x))]),
                                                      reinterpret(UInt8, [Int32(mtype)]), x)
pack(mtype::Integer, str::AbstractString) = pack(mtype, codeunits(str))
pack(mtype::Integer) = pack(mtype, UInt8[])

"""
    send

Send an IPC message to an i3 session.
"""
send(io::IO, mtype::Integer, x) = write(io, pack(mtype, x))
send(io::IO, mtype::Integer) = write(io, pack(mtype))
# below method is convenient when defining other methods
send(io::IO, mtype::Integer, ::Nothing) = send(io, mtype)
send(cnxn::Connection, mtype::Integer, x) = send(cnxn.sock, mtype, x)
send(cnxn::Connection, mtype::Integer) = send(cnxn.sock, mtype)

"""
    messagemeta

Extract metadata from and i3 IPC message.  Valid messages start with the bytes `b"i3-ipc"` followed by the message body
as dictionary parsed from JSON.
"""
function messagemeta(m::AbstractVector{UInt8})
    if length(m) < MAGIC_BYTES_LENGTH
        throw(InvalidMessageError("message \"$m\" is smaller than the size of required magic bytes $MAGIC_BYTES"))
    end
    if m[1:MAGIC_BYTES_LENGTH] ≠ MAGIC_BYTES
        throw(InvalidMessageError("expected message to have i3 magic bytes: $MAGIC_BYTES"))
    end
    ℓ = only(reinterpret(Int32, m[(MAGIC_BYTES_LENGTH+1):(MAGIC_BYTES_LENGTH+4)]))
    type = only(reinterpret(Int32, m[(MAGIC_BYTES_LENGTH+5):MESSAGE_META_LENGTH]))
    type, ℓ
end

"""
    receivebytes(cnxn)

Receive an IPC message from the i3 session as a `Vector{UInt8}`.  Returns `type, v` where `type` is the message type
and `v` is the message body.
"""
function receivebytes(io::IO)
    m = read(io, MESSAGE_META_LENGTH)
    type, ℓ = messagemeta(m)
    type, read(io, ℓ)
end
receivebytes(cnxn::Connection) = receivebytes(cnxn.sock)

"""
    receive(cnxn)

Receive a message from the i3 session as a JSON.  Returns `type, v` where `type` is the message type and `v` is the
message body.
"""
function receive(io::IO)
    type, m = receivebytes(io)
    type, JSON3.read(m)
end
receive(cnxn::Connection) = receive(cnxn.sock)

"""
    sendreceivebytes(cnxn, mtype, x=nothing)

Send a message of type `mtype` with body `x` (no body will be included if `isnothing(x)`) to the i3 session and wait for
a response, returning the response as a `Vector{UInt8}`.
"""
function sendreceivebytes(cnxn::Connection, mtype::Integer, x=nothing)
    send(cnxn, mtype, x)
    _, o = receivebytes(cnxn)
    o
end
sendreceivebytes(mtype::Integer, x=nothing) = sendreceivebytes(defaultconnection(), mtype, x)

"""
    sendreceive(cnxn, mtype, x=nothing)

Send a message of type `mtype` with body `x` (no body will be included if `isnothing(x)`) to the i3 session and wait for
a response, returning the response as a dictionary parse from JSON.
"""
sendreceive(cnxn::Connection, mtype::Integer, x=nothing) = JSON3.read(sendreceivebytes(cnxn, mtype, x))
sendreceive(mtype::Integer, x=nothing) = sendreceive(defaultconnection(), mtype, x)
