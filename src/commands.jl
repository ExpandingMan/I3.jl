
"""
    command([cnxn,] cmd)

Execute i3 command `cmd`.  If no connection is provided, the default will be used.
"""
function command(cnxn::Connection, str::AbstractString)
    r = only(sendreceive(cnxn, 0, str))
    r["success"] || throw(CommandError(r["error"]))
    nothing
end
command(str::AbstractString) = command(defaultconnection(), str)

function _critaria_arg(k, v)
    isempty(v) && return string(k)
    string(k)*"="*"\"$v\""
end

"""
    criteria_string(args)

Construct the string for specifying the [command criteria](https://i3wm.org/docs/userguide.html#command_criteria) to an
i3 command.  `args` should be in iterable of key-value pairs or 2-tuples.
"""
function criteria_string(args)
    "["*join((_critaria_arg(k, v) for (k,v) ∈ args), " ")*"]"
end

focus_string(arg::AbstractString; kwargs...) = criteria_string(kwargs)*" focus "*arg

"""
    focus([cnxn, arg]; kwargs...)

Focus a window.  `arg` should be a string, valid options are `"left", "right", "up", "down", "parent", "child", "next"
"prev"`.  Keyword arguments can be valid [command criteria](https://i3wm.org/docs/userguide.html#command_criteria).

## Examples
```julia
I3.focus("right")  # moves focus to window to the right
I3.focus(class="firefox")  # focuses the window in which firefox is running
```
"""
focus(cnxn::Connection, arg::AbstractString; kwargs...) = command(cnxn, focus_string(arg; kwargs...))
focus(arg::AbstractString; kwargs...) = focus(defaultconnection(), arg; kwargs...)
focus(cnxn::Connection; kwargs...) = focus(cnxn, ""; kwargs...)
focus(;kwargs...) = focus(defaultconnection(); kwargs...)

exec_string(arg::AbstractString; no_startup_id::Bool=false) = "exec "*(no_startup_id ? "--no-startup-id " : "")*"\""*arg*"\""

"""
    exec([cnxn,] arg; no_startup_id=false)

Executes the command given by the string (or `Cmd`) `arg`.  If `no_startup_id`, the resulting new window may not
open in the same workspace or move thet mouse cursor.

Note that the argument `arg` will automatically be enclosed with quotes when sent to `i3`, so outer quites are not necessary.
"""
exec(cnxn::Connection, arg::AbstractString; no_startup_id::Bool=false) = command(cnxn, exec_string(arg; no_startup_id))
exec(cnxn::Connection, arg::Cmd; no_startup_id::Bool=false) = exec(cnxn, strip(string(arg), '`'); no_startup_id)
exec(arg; no_startup_id::Bool=false) = exec(defaultconnection(), arg; no_startup_id)

split_string(arg::AbstractString) = "split "*arg

"""
    split([cnxn,] arg)

Split the current window (i.e. so that new windows will be contained in the current windows area).

Valid arguments are `"vertical", "horizontal", "toggle"`.
"""
split(cnxn::Connection, arg::AbstractString) = command(cnxn, split_string(arg))
split(arg::AbstractString) = split(defaultconnection(), arg)

layout_string(arg::AbstractString; toggle::Bool=false) = "layout "*(toggle ? "toggle " : "")*arg

"""
    layout([cnxn,] arg; toggle=false)

Change the current window layout.  Valid optiosn are `"default", "tabbed", "stacking", "splitv", "splith"`.
"""
layout(cnxn::Connection, arg::AbstractString; toggle::Bool=false) = command(cnxn, layout_string(arg; toggle))
layout(arg::AbstractString; toggle::Bool=false) = layout(defaultconnection(), arg; toggle)

fullscreen_string() = "fullscreen toggle"

"""
    fullscreen_string([cnxn])

Toggle fullscreen of the current window.
"""
fullscreen(cnxn::Connection=defaultconnection()) = command(cnxn, fullscreen_string())

floating_string() = "floating toggle"

"""
    floating([cnxn])

Toggle the current window between floating and tiled.
"""
floating(cnxn::Connection=defaultconnection()) = command(cnxn, floating_string())


move_string(arg::AbstractString, x::Integer=-1) = "move "*arg*(x ≥ 0 ? " $x px" : "")

"""
    move([cnxn,] arg [, x])

Move the current window.  Valid arguments are `"up", "down", "left", "right"`.  If an integer
`x` is provided, window will be moved by this many pixels.
"""
move(cnxn::Connection, arg::AbstractString, x::Integer=-1) = command(cnxn, move_string(arg, x))
move(arg::AbstractString, x::Integer=-1) = move(defaultconnection(), arg, x)

function moveposition_string(arg::AbstractString, x::Integer=-1; absolute::Bool=false)
    "move position "*(absolute ? "absolute " : "")*arg*(x ≥ 0 ? " $x px" : "")
end
moveposition_string(x::Integer, y::Integer) = "move position $x px $y px"

"""
    moveposition([cnxn,] arg [, x]; absolute=false)
    moveposition(x, y)

Move the current window.  Valid arguments are `"up", "right", "left", "down", "center", "mouse"`.
"""
moveposition(cnxn::Connection, x::Integer, y::Integer) = command(cnxn, moveposition_string(x, y))
moveposition(x::Integer, y::Integer) = moveposition(defaultconnection(), x, y)
function moveposition(cnxn::Connection, arg::AbstractString, x::Integer=-1; absolute::Bool=false)
    moveposition(cnxn, arg, x; absolute)
end
function moveposition(arg::AbstractString, x::Integer=-1; absolute::Bool=false)
    moveposition(defaultconnection(), arg, x; absolute)
end

mark_string(arg::AbstractString) = "mark "*arg

"""
    mark([cnxn,] arg)

Marks the current container with the name `arg`.
"""
mark(cnxn::Connection, arg::AbstractString) = command(cnxn, mark_string(arg))
mark(arg::AbstractString) = mark(defaultconnection(), arg)

unmark_string() = "unmark"

"""
    unmark([cnxn])

Unmark the current container.
"""
unmark(cnxn::Connection=defaultconnection()) = command(cnxn, unmark_string())

restart_string() = "restart"

"""
    restart([cnxn])

Restart the i3 session.
"""
restart(cnxn::Connection=defaultconnection()) = command(cnxn, restart_string())

reload_string() = "reload"

"""
    reload([cnxn])

Reload the i3 session.
"""
reload(cnxn::Connection=defaultconnection()) = command(cnxn, reload_string())

exit_string() = "exit"

"""
    exit([cnxn])

Exit the i3 session.
"""
exit(cnxn::Connection=defaultconnection()) = command(cnxn, exit_string())


"""
    marks([cnxn])

Get list of marks.
"""
marks(cnxn::Connection=defaultconnection()) = sendreceive(cnxn, messageindex(:get_marks))

"""
    config([cnxn])

Get the currently loaded i3 config file.
"""
config(cnxn::Connection=defaultconnection()) = sendreceive(cnxn, messageindex(:get_config))
