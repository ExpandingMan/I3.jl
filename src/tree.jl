
"""
    Node

A node in an i3 tree.
"""
mutable struct Node
    id::Integer
    type::String
    scratchpad_state::String
    percent::Union{Nothing,Float64}
    urgent::Bool
    focused::Bool
    layout::Symbol
    workspace_layout::String
    last_split_layout::String
    border::String
    current_border_width::Int64
    rect::Rectangle
    deco_rect::Rectangle
    window_rect::Rectangle
    geometry::Rectangle
    name::String
    window::Union{Nothing,Int64}
    window_type::Union{Nothing,String}
    nodes::Vector{Node}
    floating_nodes::Vector{Node}
    focus::Vector{Int64}
    fullscreen_mode::Int64
    sticky::Bool
    floating::String
    orientation::String
    marks::Vector{String}
    swallows::Vector{Dict{String,Any}}
end

_empty_string_nothing(d::DictOrNamedTuple, k) = isnothing(get(d, k, nothing)) ? "" : get(d, k, "")

function Node(d::DictOrNamedTuple)
    type = _empty_string_nothing(d, :type)
    name = _empty_string_nothing(d, :name)
    Node(d[:id], type, d[:scratchpad_state], d[:percent], d[:urgent], d[:focused], Symbol(d[:layout]),
         d[:workspace_layout], d[:last_split_layout], d[:border], d[:current_border_width],
         Rectangle(d[:rect]), Rectangle(d[:deco_rect]), Rectangle(d[:window_rect]), Rectangle(d[:geometry]),
         name, d[:window], d[:window_type], Node.(d[:nodes]), Node.(d[:floating_nodes]),
         d[:focus], d[:fullscreen_mode], d[:sticky], d[:floating], d[:orientation], d[:marks],
         d[:swallows],
        )
end
Node(d::AbstractDict{<:AbstractString}) = Node(Dict(Symbol(k)=>v for (k,v) ∈ d))

Base.keys(::Node) = fieldnames(Node)
Base.values(n::Node) = ntuple(i -> getfield(n, i), nfields(n))

Base.Dict(n::Node) = Dict(ϕ=>getfield(n, ϕ) for ϕ ∈ fieldnames(typeof(n)))

StructTypes.StructType(::Type{Node}) = StructTypes.DictType()
StructTypes.keyvaluepairs(n::Node) = Dict(k=>getproperty(n, k) for k ∈ propertynames(Node))

function _show_node_string(io::IO, name::AbstractString)
    show(io, Node)
    print(io, "(")
    printstyled(io, "\""*name*"\"", color=:yellow)
    print(io, ")")
    nothing
end
_show_node_string(io::IO, n::Node) = _show_node_string(io, n.name)

Base.show(io::IO, n::Node) = _show_node_string(io, n)

function Base.show(io::IO, ::MIME"text/plain", n::Node; kw...)
    print_tree(_show_node_string, io, n; kw...)
end



"""
    tree([cnxn])

Get the root node of the i3 session tree.  The entire tree is descended from the root node.
"""
tree(::Type{𝒯}, cnxn::Connection) where {𝒯} = JSON3.read(sendreceivebytes(cnxn, messageindex(:get_tree)), 𝒯)
tree(cnxn::Connection) = tree(Node, cnxn)
tree(::Type{𝒯}) where {𝒯} = tree(𝒯, defaultconnection())
tree() = tree(defaultconnection())

"""
    children(n::Node)

Get the immediate children of the i3 tree node.
"""
AbstractTrees.children(n::Node) = n.nodes

"""
    child(n::Node, i)

Get the child of `n` at index `i`.
"""
child(n::Node, i::Integer) = children(n)[i]

"""
    haschildren(n::Node)

Returns `true` if the i3 tree node has immediate children, else `false`.
"""
haschildren(n::Node) = !isempty(children(n))

"""
    leaves(n::Node)

Get all leaves descended from the i3 tree node `n`.  That is, only nodes which are descended from `n` but which do not
themsleves have any children will appear in the returned list.
"""
leaves(n::Node) = collect(Leaves(n))
leaves() = leaves(tree())

"""
    descendants(n::Node)

Returns an iterator of all descendants of the `i3` `Node`.
"""
descendants(n::Node) = collect(PostOrderDFS(n))

"""
    descendants(𝒻, n::Node)

Return the descendants of node `n` for which `f(n) == true`.
"""
descendants(𝒻, n::Node) = filter!(𝒻, descendants(n))

"""
    descendants(𝒻)

Get the descendants of the root node of the current tree for which `𝒻` returns `true`.
"""
descendants(𝒻) = descendants(𝒻, tree())

"""
    descendants()

Gets all descendants of the current tree.  This is just like `tree()`, except that it returns the tree
flattened.
"""
descendants() = descendants(tree())

"""
    isfocused(n)

Whether the node is the currently focused window.
"""
isfocused(n::Node) = n.focused

"""
    isleaf(n)

Whether the node is a leaf, i.e. has no descendants other than itself.
"""
isleaf(n::Node) = isempty(n.nodes)

"""
    getfocused(cnxn::Connection)

Get the currently focused window.  Returns `nothing` if there isn't one.
"""
function getfocused(cnxn::Connection)
    ns = descendants(isfocused, tree(cnxn))
    isempty(ns) ? nothing : first(ns)
end
getfocused() = getfocused(defaultconnection())


"""
    iswindow(n)

Determines whether a node is a "normal" window.  This should imply that the node is drawn on screen provided
the workspace containing it is visible.  Determining whether the node is actually visible is more expensive,
see [`isvisible`](@ref).

**Note:** This does not find floating windows.
"""
iswindow(n::Node) = isleaf(n) && n.type == "con" && n.rect.Δx > 0 && n.rect.Δy > 0

"""
    isnormalwindow(n)

Returns true if `iswindwo(n)` and the window type is `"normal"`.  These are "normal" windows in (arguably) the
most obvious sense and should exclude toolbars, notifications and docks.

**Note:** This does not find floating windows.
"""
isnormalwindow(n::Node) = iswindow(n) && n.window_type == "normal"

#====================================================================================================
       TODO:

`isvisible` is looking complicated: first you have to look at the tree to figure out which
workspace the node belongs to, then you have to call `workspaces()` to determine whether
the workspace itself is visible.
====================================================================================================#
