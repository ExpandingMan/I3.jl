module I3

using Sockets, JSON3, StructTypes, AbstractTrees

#using Debugger

const DictOrNamedTuple = Union{AbstractDict,NamedTuple}

const MAGIC_BYTES = b"i3-ipc"
const MAGIC_BYTES_LENGTH = length(MAGIC_BYTES)
const MESSAGE_META_LENGTH = MAGIC_BYTES_LENGTH + 8


const MSG_TYPES = [:run_command,
                   :get_workspaces,
                   :subscribe,
                   :get_outputs,
                   :get_tree,
                   :get_marks,
                   :get_bar_config,
                   :get_version,
                   :get_binding_modes,
                   :get_config,
                   :send_tick,
                   :sync
                  ]
const MSG_TYPE_INDICES = Dict{Symbol,Int}(MSG_TYPES .=> ((1:length(MSG_TYPES)) .- 1))

messageindex(s::Symbol) = MSG_TYPE_INDICES[s]


struct InvalidMessageError <: Exception
    msg::String
end

struct CommandError <: Exception
    msg::String
end


include("ipc.jl")

const DEFAULT_CONNECTION = Ref{Connection}()

"""
    defaultconnection()

Get the cached connection to the default i3 session.
"""
defaultconnection() = DEFAULT_CONNECTION[]

"""
    defaultconnection!()

Set the cached connection to the default i3 session.  This sets a global variable which is accessible through
`defaultconnection()`.
"""
function defaultconnection!()
    spath = try
        socketpath()
    catch e
        if e isa Base.IOError
            nothing
        else
            rethrow(e)
        end
    end
    isnothing(spath) && return
    DEFAULT_CONNECTION[] = Connection(spath)
end


include("workspace.jl")
include("tree.jl")
include("commands.jl")
include("server.jl")


__init__() = defaultconnection!()

end
