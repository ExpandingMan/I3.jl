
abstract type AbstractHandler end

struct MockHandler <: AbstractHandler
    workspaces::Vector{Workspace}
    outputs::Vector{Output}
    tree::Union{Nothing,Node}
end


function respond(h::AbstractHandler, io::IO)
    mtype, m = receivebytes(io)
    r = response(h, mtype, m)
    @debug("i3 server got message, will respond", mtype, String(copy(m)), r)
    send(io, mtype, r)
end

response(h::AbstractHandler, mtype::Integer, m) = response(h, Val{Int64(mtype)}, m)

response(h::MockHandler, ::Type{Val{messageindex(:run_command)}}, m) = JSON3.write([Dict("success"=>true)])

response(h::MockHandler, ::Type{Val{messageindex(:get_workspaces)}}, m) = JSON3.write(h.workspaces)

response(h::MockHandler, ::Type{Val{messageindex(:get_outputs)}}, m) = JSON3.write(h.outputs)

"""
    serve(f, sockname)

Create a server at socket `sockname`.  Once the socket is created, the server will block the thread until a single
connection is accepted.  Once this occurs, the server will repeatedly execute the function `f` on the IO stream from the
socket.

To run this asyncronously use `@async`.  To run on a new CPU thread, use `Threads.@spawn`.
"""
function serve(f::Function, sockname::AbstractString)
    s = listen(sockname)
    @info("listening on socket \"$sockname\"")
    io = accept(s)
    @info("server on socket \"$sockname\" accepted connection")
    while !eof(io)
        try
            f(io)
        catch e
            @warn("server on socket \"$sockname\" experienced an error", exception=(e, catch_backtrace()))
        end
    end
    isopen(io) && close(io)
    @info("client disconnected, connection on socket \"$sockname\" closed")
end

serve(h::AbstractHandler, sockname::AbstractString) = serve(io -> respond(h, io), sockname)
