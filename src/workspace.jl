
"""
    Rectangle

A rectangle on a screen within an i3 session.

## Fields
- `x`: ``x`` position in pixels.
- `y`: ``y`` position in pixels.
- `Δx`: Extent in ``x`` in pixels.  Referred to by i3 IPC as `width`.
- `Δy`: Extent in ``y`` in pixels.  Referred to by i3 IPC as `height`.
"""
struct Rectangle
    x::Int
    y::Int
    Δx::Int
    Δy::Int
end
Rectangle(d::DictOrNamedTuple) = Rectangle(d[:x], d[:y], d[:width], d[:height])
Rectangle(d::AbstractDict{<:AbstractString}) = Rectangle(Dict(Symbol(k)=>v for (k,v) ∈ d))

Base.:(==)(a::Rectangle, b::Rectangle) = all(getfield(a, ϕ) == getfield(b, ϕ) for ϕ ∈ 1:nfields(a))

StructTypes.StructType(::Type{Rectangle}) = StructTypes.DictType()
StructTypes.keyvaluepairs(r::Rectangle) = Dict(:x=>r.x, :y=>r.y, :width=>r.Δx, :height=>r.Δy)


"""
    Output

Data structure describing an output device.
"""
mutable struct Output
    name::String
    active::Bool
    primary::Bool
    rect::Rectangle
    workspace::Union{Nothing,String}
end
Output(d::DictOrNamedTuple) = Output(d[:name], d[:active], d[:primary], Rectangle(d[:rect]), d[:current_workspace])
Output(d::AbstractDict{<:AbstractString}) = Output(Dict(Symbol(k)=>v for (k,v) ∈ d))

Base.:(==)(a::Output, b::Output) = all(getfield(a, ϕ) == getfield(b, ϕ) for ϕ ∈ 1:nfields(a))

StructTypes.StructType(::Type{Output}) = StructTypes.DictType()
StructTypes.keyvaluepairs(o::Output) = Dict(:name=>o.name, :active=>o.active, :primary=>o.primary, :rect=>o.rect,
                                            :current_workspace=>o.workspace)

Base.show(io::IO, o::Output) = write(io, "Output(\""*o.name*"\")")

function Base.show(io::IO, ::MIME"text/plain", o::Output)
    show(io, Output)
    print(io, "(")
    printstyled(io, "\""*o.name*"\"", color=:yellow)
    print(io, ", ")
    show(io, o.rect)
    print(io, " )")
    o.primary && printstyled(io, " primary", color=:blue, bold=true)
    o.active && printstyled(io, " active", color=:green, bold=true)
    isnothing(o.workspace) || printstyled(io, " [active workspace: $(o.workspace)]", color=:blue)
    nothing
end


"""
    Workspace

Data structure describing and i3 workspace.
"""
mutable struct Workspace
    id::Int
    num::Int
    name::String
    visible::Bool
    focused::Bool
    rect::Rectangle
    output::String
    urgent::Bool
end
Workspace(d::DictOrNamedTuple) = Workspace(d[:id], d[:num], d[:name], d[:visible], d[:focused], Rectangle(d[:rect]),
                                           d[:output], d[:urgent])
Workspace(d::AbstractDict{<:AbstractString}) = Workspace(Dict(Symbol(k)=>v for (k,v) ∈ d))

Base.:(==)(a::Workspace, b::Workspace) = all(getfield(a, ϕ) == getfield(b, ϕ) for ϕ ∈ 1:nfields(a))

StructTypes.StructType(::Type{Workspace}) = StructTypes.DictType()
StructTypes.keyvaluepairs(w::Workspace) = Dict(:id=>w.id, :num=>w.num, :name=>w.name, :visible=>w.visible,
                                               :focused=>w.focused, :rect=>w.rect, :output=>w.output, :urgent=>w.urgent)

function Base.show(io::IO, w::Workspace)
    show(io, Workspace)
    print(io, "(")
    printstyled(io, "\""*w.name*"\"", color=:yellow)
    print(io, ", output=")
    printstyled(io, "\""*w.output*"\"", color=:yellow)
    print(io, ")")
    nothing
end

function Base.show(io::IO, ::MIME"text/plain", w::Workspace)
    show(io, w)
    w.visible && printstyled(io, " visible", color=:cyan)
    w.focused && printstyled(io, " focused", bold=true, color=:blue)
    nothing
end


"""
    outputs([cnxn])

Retrieve a list of outputs on the i3 server accessible at connection `cnxn`.
"""
outputs(::Type{𝒯}, cnxn::Connection) where {𝒯} = JSON3.read(sendreceivebytes(cnxn, messageindex(:get_outputs)), 𝒯)
outputs(cnxn::Connection) = outputs(Vector{Output}, cnxn)
outputs(::Type{𝒯}) where {𝒯} = outputs(𝒯, defaultconnection())
outputs() = outputs(defaultconnection())

"""
    output([cnxn,] name)

Retrieve the output with name `name`.  Returns `nothing` if it does not exist.
"""
function output(f::Function, cnxn::Connection)
    os = outputs(cnxn)
    os[findfirst(f, os)]
end
output(cnxn::Connection, name::AbstractString) = output(o -> o.name == name, cnxn)
output(name::AbstractString) = output(defaultconnection(), name)

"""
    workspaces([cnxn])

Retrieve a list of workspaces on the i3 server accessible at connection `cnxn`.
"""
workspaces(::Type{𝒯}, cnxn::Connection) where {𝒯} = JSON3.read(sendreceivebytes(cnxn, messageindex(:get_workspaces)), 𝒯)
workspaces(cnxn::Connection) = workspaces(Vector{Workspace}, cnxn)
workspaces(::Type{𝒯}) where {𝒯} = workspaces(𝒯, defaultconnection())
workspaces() = workspaces(defaultconnection())

"""
    workspace([cnxn,] name)

Retrieve the workspace with name `name`.  Returns `nothing` if it does not exist.
"""
function workspace(f::Function, cnxn::Connection)
    ws = workspaces(cnxn)
    ws[findfirst(f, ws)]
end
workspace(cnxn::Connection, name::AbstractString) = workspace(w -> w.name == name, cnxn)
workspace(name::AbstractString) = workspace(defaultconnection(), name)
