# I3

[![docs](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/I3.jl/)
[![build status](https://img.shields.io/gitlab/pipeline/ExpandingMan/I3.jl/master?style=for-the-badge)](https://gitlab.com/ExpandingMan/I3.jl/-/pipelines)

Interact with the [i3](https://i3wm.org/) window manager in Julia.  The i3 IPC
specification can be found [here](https://i3wm.org/docs/ipc.html).

