```@meta
CurrentModule = I3
```

# I3

This package provides a Julia interface to the [i3](https://i3wm.org/) window manager.

## Installation
I hope to register this package soon, but in the meantime, you can do
```julia
Pkg.add("https://gitlab.com/ExpandingMan/I3.jl")
```
or do `]add https://gitlab.com/ExpandingMan/I3.jl` in the REPL.

Presumably if you want to use this you already have i3 installed.  In principle this
package can be used regardless of whether i3 is running, but it won't do you much good
unless you have an active i3 session.


## Usage
You will likely find `I3.command` to be the most useful function.  This executes i3
commands as they would appear in the config or via the `i3-msg` command.
```julia
I3.command("focus right")  # focus window to the right
```
A list of valid i3 command can be found
[here](https://i3wm.org/docs/userguide.html#list_of_commands).

### Commands
Aliases are available for many common commands
```julia
I3.focus("right")  # focuses the window to the right
I3.focus(class="firefox")  # focus container running firefox

I3.exec("julia", no_startup_id=true)  # start up Julia without binding to current workspace

I3.split("vertical")  # split the current window vertically

I3.fullscreen()  # toggle fullscreen on current window

I3.mark("test_container")  # mark the current container

I3.unmark()  # unmark current container

I3.config()  # fetch the currently loaded config

I3.reload()  # reload config

I3.restart()  # restart i3

I3.exit()  # exit i3
```

!!! note

    Most functions also accept an `I3.Connection` as the first argument, allowing you to
    specify a non-default i3 session connection.


### Inspecting the Tree
This package also offers some helpful introspection tools.  To get a list of outputs do
```julia
I3.outputs()
```
This will provide a list of Julia data structures describing the outputs.

Similarly, you can get a list of workspace data structures by doing
```julia
I3.workspaces()
```

You can get the i3 session tree by doing
```julia
t = I3.tree()

I3.children(t)  # get immediate children of tree root node

I3.leaves(t)  # get all leaves of i3 tree

I3.descendants(t)  # get all descendants of i3 tree

I3.descendants()  # same as above, calling tree() when called
```

You may find [QuickMenus.jl](https://gitlab.com/ExpandingMan/QuickMenus.jl) useful id looking for
specific nodes of the tree.

For example
```julia
I3.descendants(t) |> collect |> fzf
```
will bring up an [`fzf`](https://github.com/junegunn/fzf) dialog for selecting a node.

The `descendants` function optionally accepts a filter function.  This can be used to obtain lists
of windows with specific properties.  For example
```julia
using I3: descendants, iswindow, isnormalwindow

descendants(iswindow)  # roughly speaking, these are all things that are visible when their workspace is

descendants(isnormalwindow)  # same as above excluding toolbars, docks and notifications
```
