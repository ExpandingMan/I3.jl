using I3
using Documenter

makedocs(;
    modules=[I3],
    authors="Expanding Man <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/I3.jl/blob/{commit}{path}#{line}",
    sitename="I3.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/I3.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
        "API" => "api.md"
    ],
)

deploydocs(;
    repo="gitlab.com/ExpandingMan/I3.jl",
)
